from PyQt4 import QtCore, QtGui
from PyQt4.Qt import QMutex
from PyQt4.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from PyQt4 import QtOpenGL

import pyqtgraph as pg
#from pyqtgraph.Qt import QtCore, QtGui

import sys
import numpy as np

import main_window

from data_recorder import *
from scope import *

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class App(QtGui.QMainWindow, main_window.Ui_MainWindow):
	def __init__(self, parent=None):
		super(App, self).__init__(parent)
		self.setupUi(self) 
		#self.MainStateMachine()
		self.InitializeScopes()

	def InitializeScopes(self):
		self.graphicsView.addPlot(enableMenu=False)
		self.graphicsView.nextRow()
		self.graphicsView.addPlot(enableMenu=False)

	def MainStateMachine(self):
		while True:
			continue


if __name__=="__main__":
	app = QtGui.QApplication(sys.argv)
	thisapp = App()
	thisapp.show()
	app.exec_()
	sys.exit()