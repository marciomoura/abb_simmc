# protocol.py

# data size
SIMMC_PROT_FRAME_SIZE = 256

SIMMC_PROT_BOF = 0xC1
SIMMC_PROT_EOF = 0xC0

# service ids
SIMMC_SERIVCE_ID_PAR = 0x10
SIMMC_SERVICE_ID_PAR_INFORM = 0x10
SIMMC_SERVICE_ID_PAR_TRANSFER = 0x11 


SIMMC_SERVICE_ID_LIST = [0x10]

#-----------------------------------------------
# BOF | SERVICE | LENGTH | DATA          | EOF -
#-----------------------------------------------

'''
fixed-size frame/up to a certain size 

1 - Get the payload data. If anything is wrong, packet gets ignored
########################
1 - check bof 
2 - check service id
3 - check data length 
4 - get payload data - [according to data length]
5 - get eof 
########################

2 - With the correct payload, go to the corresponding service 

 - 0x11 : Inform Available Data 
 	[ DATA ID, SCALING, TYPE, NAME ]

 	- The class can create or spand the information regardin data available 
 		for the interface to read it.

 - 0x12 : Transmit Group of Data 
 	[ DATA ID, TICK, DATA VALUE ]

 	- Retrieve the data and place them on their structures 
 		Data ID 1: [Value,Tick]; [Value,Tick]... 
 		Data ID 2: [Value,Tick]; [Value,Tick]...
'''

class DataRecorder(object):
	"""docstring for DataRecorder"""
	def __init__(self):
		super(DataRecorder, self).__init__()
		self.list_len = 0
		self.inserted_elements = 0
		self.data_list = []
		self.tick_list = []

	def SetListLen(self, len):
		if self.list_len > len:
			self.list_len = len
			self.RemoveExcessData()
		else:
			self.list_len = len
		return

	def RemoveData(self):
		self.inserted_elements -= 1
		self.data_list.pop(0)
		self.tick_list.pop(0)
	
	def RemoveExcessData(self):
		while self.inserted_elements > self.list_len:
			self.RemoveData()

	def ClearList(self):
		self.data_list.clear()
		self.tick_list.clear()
		return

	def InsertData(self, data, tick):
		self.inserted_elements += 1
		self.data_list.append(data)
		self.tick_list.append(tick)
		self.RemoveExcessData()

	def GetDataList(self):
		return self.data_list.copy()

	def GetTickList(self):
		return self.tick_list.copy()

	def GetLists(self):
		return (self.data_list.copy(),self.tick_list.copy())


class RecorderManager(object):
	def __init__(self):
		super(RecorderManager, self).__init__()
		self.recorder_list= []

	def InsertRecorder(self, id):
		recorder_handler = {"id" : id, "recorder" : DataRecorder() }
		self.recorder_list.append(recorder_handler)
		return

	def RemoveRecorder(self, id):
		for data_rec in self.recorder_list:
			if data_rec["id"] == id:
				index = self.recorder_list.index(data_rec)
				self.recorder_list.pop(index)
				return

	def GetIndexById(self, id):
		for index, rec_dict in enumerate(self.recorder_list):
			if rec_dict["id"] == id:
				return index
		return -1 

	def GetDataRecorderById(self, id):
		index = self.GetIndexById(id)
		if index >= 0:
			recorder = self.recorder_list[index]["recorder"]
			return recorder 
		return False 

	def SetDataRecorderBufferLen(self, id, length):
		if length > 0:
			data_recorder = self.GetDataRecorderById(id)
			if data_recorder != False:
				data_recorder.SetListLen(length)
				return True 	
		return False

	def InsertData(self, id, data, tick):
		data_recorder = self.GetDataRecorderById(id)
		if data_recorder != False:
			data_recorder.InsertData(data,tick)
			return True 
		return False

	def GetDataById(self, id):
		data_recorder = self.GetDataRecorderById(id)
		if data_recorder != False:
			return data_recorder.GetLists()
		return False

	# testing methods
	def PrintRecorder(self):
		for i in self.recorder_list:
			print(i)
		return



