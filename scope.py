"""
UDP TRANSPORT -> PROTOCOL -> DATA RECORDERD -> SCOPE
A Scope has a reference to the data recorder object.
You can retrieve multiple data from different recorders
to be displayed at different scopes.



DATA RECORDER LIST [ DATA 0, DATA 1, DATA 2, DATA 3, ... ]
Each data has a unique signal id.

Scope will hold the signal ids to retrieve data from the recorder list.
"""

SCOPE_MAX_VIEWER_SIGNALS = 5


# the scope will have the reference of the object
class Scope(object):
	"""docstring for Scope"""
	def __init__(self, scope_id):
		super(Scope, self).__init__()
		self.max_viewer_signals = SCOPE_MAX_VIEWER_SIGNALS
		self.id = scope_id
		self.signal_id_list = []

	# Limits, range....

	def InsertSignal(self, signal_id):	
		if len(self.signal_id_list) < self.max_viewer_signals:
			if signal_id not in self.signal_id_list:
				self.signal_id_list.append(signal_id)
				return True 
		return False
	
	def RemoveSignal(self, signal_id):
		if signal_id in self.signal_id_list:
			index = self.signal_id_list.index(signal_id)
			self.signal_id_list.pop(index)
			return True 	
		return False

	def GetSignals(self):
		return self.signal_id_list
